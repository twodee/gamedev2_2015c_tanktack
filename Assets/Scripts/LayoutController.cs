﻿using UnityEngine;
using System.Collections;

public class LayoutController : MonoBehaviour {
  public TextAsset layout;
  public GameObject tank;
  public Color[] playerColors = new Color[2];
  public string nextLevel;

  public static LayoutController S;
  private float width;
  private float height;

  public float right {
    get { return 0.5f * width; }
  }
  public float top {
    get { return 0.5f * height; }
  }

  void Awake() {
    S = this;
  }

  void Start() {
    Camera camera = Camera.main;
    height = camera.orthographicSize * 2;
    width = camera.aspect * height;

    string[] lines = layout.text.Split('\n');
    int nrows = lines.Length - 1;
    int ncols = lines[0].Length;

    float blockWidth = width / ncols;
    float blockHeight = height / nrows;

    GameObject walls = GameObject.Find("/Walls");

    for (int r = 0; r < nrows; ++r) {
      for (int c = 0; c < ncols; ++c) {
        Vector3 center = new Vector3((c + 0.5f) * blockWidth - 0.5f * width, (r + 0.5f) * blockHeight - 0.5f * height, 0.0f);

        if (lines[r][c] == '#') {
          GameObject block = GameObject.CreatePrimitive(PrimitiveType.Cube);
          block.transform.position = center;
          block.transform.localScale = new Vector3(blockWidth, blockHeight, 1.0f);
          DestroyImmediate(block.GetComponent<BoxCollider>());
          block.AddComponent<BoxCollider2D>();
          block.transform.parent = walls.transform;
        }

        else if (lines[r][c] == '1' || lines[r][c] == '2') {
          int id = lines[r][c] - '0';
          GameObject instance = (GameObject) Instantiate(tank, center, Quaternion.identity);
          instance.GetComponent<TankController>().playerID = id;
          instance.GetComponent<SpriteRenderer>().color = playerColors[id - 1];
          Sprite tankSprite = instance.GetComponent<SpriteRenderer>().sprite;
          float tankAspect = tankSprite.bounds.size.x / (float) tankSprite.bounds.size.y;
          print(tankAspect);
          float blockAspect = blockWidth / blockHeight;
          float tankScale;
          if (tankAspect > blockAspect) {
            tankScale = blockWidth / tankSprite.bounds.size.x;
          } else {
            tankScale = blockHeight / tankSprite.bounds.size.y;
          }
          instance.transform.localScale = new Vector3(0.5f * tankScale, 0.5f * tankScale, 1.0f);
          instance.name = "Tank " + id;
        }
      }
    }
  }

  public void GoToNext() {
    Application.LoadLevel(nextLevel);
  }
}
