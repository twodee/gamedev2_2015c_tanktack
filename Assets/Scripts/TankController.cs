﻿using UnityEngine;
using System.Collections;

public class TankController : MonoBehaviour {
  public float torque;
  public float oomph;
  public GameObject projectile;
  public int playerID;
  public AudioClip shootClip;
  public AudioClip explodeClip;

  private new Rigidbody2D rigidbody;
  private new SpriteRenderer renderer;

  private float turn;
  private float push;

  private string horizontalAxis;
  private string verticalAxis;
  private string fireButton;
  private string layer;

  void Start() {
    rigidbody = GetComponent<Rigidbody2D>(); 
    renderer = GetComponent<SpriteRenderer>(); 

    horizontalAxis = "Horizontal" + playerID;
    verticalAxis = "Vertical" + playerID;
    fireButton = "Fire" + playerID;
    layer = "Player" + playerID;

    gameObject.layer = LayerMask.NameToLayer(layer);
  }
  
  void Update() {
    turn = Input.GetAxis(horizontalAxis);
    push = Input.GetAxis(verticalAxis);

    if (Input.GetButton(fireButton)) {
      GameObject instance = (GameObject) Instantiate(projectile, transform.position, transform.rotation);
      instance.GetComponent<SpriteRenderer>().color = renderer.color;
      instance.layer = gameObject.layer; //LayerMask.NameToLayer(layer);
      GetComponent<AudioSource>().PlayOneShot(shootClip);
    }
  }

  void FixedUpdate() {
    rigidbody.AddTorque(-torque * turn);
    rigidbody.AddRelativeForce(Vector3.up * oomph * push);
  }

  void OnCollisionEnter2D(Collision2D collision) {
    if (collision.gameObject.tag == "Projectile") {
      AudioSource.PlayClipAtPoint(explodeClip, transform.position);
      /* Destroy(this.gameObject); */
      gameObject.SetActive(false);
      Invoke("OnDoneDied", explodeClip.length);
    }
  }

  void OnDoneDied() {
    LayoutController.S.GoToNext();
  }
}
