﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {
  public float oomph;
  public AudioClip bounceClip;
  public AudioClip sputterClip;

  private new Rigidbody2D rigidbody;

  void Start() {
    rigidbody = GetComponent<Rigidbody2D>(); 
    rigidbody.AddRelativeForce(Vector3.up * oomph);
  }

  void OnCollisionEnter2D(Collision2D collision) {
    GetComponent<AudioSource>().PlayOneShot(bounceClip);
  }
}
