﻿using UnityEngine;
using System.Collections;

public class TeleportableController : MonoBehaviour {
  private new Rigidbody2D rigidbody;
  private static float EPSILON = 0.001f;

  void Start() {
    rigidbody = GetComponent<Rigidbody2D>();
  }

  void FixedUpdate() {
    Vector3 position = rigidbody.position;
    float top = LayoutController.S.top;
    float right = LayoutController.S.right;

    if (position.y > top) {
      position.y = -top + EPSILON;
    } else if (position.y < -top) {
      position.y = top - EPSILON;
    }

    if (position.x > right) {
      position.x = -right + EPSILON;
    } else if (position.x < -right) {
      position.x = right - EPSILON;
    }

    rigidbody.position = position;
  }
}
